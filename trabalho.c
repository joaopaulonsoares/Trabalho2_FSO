#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>

#define BUFFER 256

int main(){

    fd_set set;
    pid_t filhoPreguicoso, filhoAtivo;
    int teste,i=0;
    int status;
    struct timeval Inicio, Fim;
    double Segundos, Milissegundos, Microssegundos;
    int fd[2];

    if (pipe(fd) == -1) {
        perror("pipe");
        exit(1);
    }

     filhoPreguicoso = fork();
     //filhoAtivo = fork();
     gettimeofday(&Inicio, NULL);

     do{

         if (filhoPreguicoso == 0) {
            /* Child A code */
         	printf("filhoPreguicoso %d\n",i );
         	i++;
            /*No filho, vamos ESCREVER, então vamos fechar a LEITURA do Pipe neste lado*/
            close(fd[0]);

            char str[BUFFER] = "Aprendi a usar Pipes em C!";
            printf("String enviada pelo filho no Pipe: '%s'\n", str);

            /* Escrevendo a string no pipe */
            write(fd[1], str, sizeof(str) + 1);
            //int ale = rand() % 3;
            int ale = 3;
            //printf("Tempo: %d\n", ale);
          
            sleep(ale);
         } else {

            //int ret =0;
            //ret = select(fd[0] + 1, &set, NULL, NULL, 0);
            /* Parent Code */

                char str_recebida[BUFFER];
                //if(ret ==0){
                /* No pai, vamos ler. Então vamos fechar a entrada de ESCRITA do pipe */
                close(fd[1]);
                /* Lendo o que foi escrito no pipe, e armazenando isso em 'str_recebida' */
                read(fd[0], str_recebida, sizeof(str_recebida));

                    printf("String lida pelo pai no Pipe : '%s'\n", str_recebida);
                    printf("============================================\n");

                //}
                //fd[0]=0;
                //wait(NULL);
             	
             
         }

        gettimeofday(&Fim, NULL);
        /* Calcula o espaço de tempo */
        Segundos = Fim.tv_sec - Inicio.tv_sec;
        Microssegundos = Fim.tv_usec - Inicio.tv_usec;

    }while(Segundos!=6);

    wait(&status);

    printf("O programa demourou: %.0lf\n",Segundos);
    kill(filhoPreguicoso, SIGKILL);
	return 0;
}




/*
  int main(){

     pid_t filhoPreguicoso, filhoAtivo;
     int teste;
     int status;

     filhoPreguicoso = fork();

     if (filhoPreguicoso == 0) {
         /* Child A code *//*
        for(int i=0;i<10;i++){
          printf("filhoPreguicoso %d\n",i );
          sleep(2);
        }
        exit(0);
     } else {
         filhoAtivo = fork();

         if (filhoAtivo == 0) {
                printf("filhoAtivo\n");
                scanf("%d",&teste);
         } else {
             /* Parent Code *//*
                sleep(10);
                wait(&status);
                printf("Processo pai\n");
                return 0;
         }
     }
*//*
    return 0;
}  
*/