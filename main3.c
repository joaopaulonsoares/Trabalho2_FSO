                 /* arquivo test_fork1.c */    

/* Descritores herdados pelos processos filhos */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>

#define BUFFER 256

int main(){

    pid_t filhoPreguicoso, filhoAtivo;
    int teste,i=0;
    int status;
    struct timeval Inicio, Fim;
    double Segundos, Milissegundos, Microssegundos;
    int fd[2];

    if (pipe(fd) == -1) {
        perror("pipe");
        exit(1);
    }


     gettimeofday(&Inicio, NULL);

     do{

         if (filhoAtivo == 0) {
                 filhoAtivo = fork();
           char str[BUFFER];
            /* Child A code */
            //printf("filho Ativo %d\n",i );
                scanf("%s",str);
            /*No pai, vamos ESCREVER, então vamos fechar a LEITURA do Pipe neste lado*/
            close(fd[0]);


            printf("String enviada pelo filho no Pipe: '%lf:%.2lf'\n",Segundos,Microssegundos);

            /* Escrevendo a string no pipe */
            write(fd[1], str, sizeof(str) + 1);
         } else {
            
                char str_recebida[BUFFER];

                /* No filho, vamos ler. Então vamos fechar a entrada de ESCRITA do pipe */
                close(fd[1]);

                /* Lendo o que foi escrito no pipe, e armazenando isso em 'str_recebida' */
                read(fd[0], str_recebida, sizeof(str_recebida));
                
                    printf("String lida pelo pai no Pipe : '%.0lf:%.2lf'\n",Segundos,Microssegundos);
                    printf("============================================\n");                
             
         }

        gettimeofday(&Fim, NULL);   
        /* Calcula o espaço de tempo */
        Segundos = Fim.tv_sec - Inicio.tv_sec;
        Microssegundos =  (Fim.tv_usec - Inicio.tv_usec)/1000  ;

    }while(Segundos!=2);

    wait(&status);
    printf("O programa demourou: %.0lf:%2.3lf\n",Segundos,Microssegundos );

    return 0;
}
/*
int main()
{
    
     pid_t child_a, child_b;

     child_a = fork();

     if (child_a == 0) {
          //Child A code 
     } else {
         child_b = fork();

         if (child_b == 0) {
             // Child B code 
         } else {
             //Parent Code 
         }
     }
     
}*/