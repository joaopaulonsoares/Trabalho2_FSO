#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>

#define MICRO_PER_SECOND 1000 

int main(){

int i=0; 

	struct timeval Inicio, Fim;
    double Segundos, Milissegundos, Microssegundos;

    /* Coleta a data de inicio do processo */
    gettimeofday(&Inicio, NULL);

    /*
     * Seu programa faz alguma coisa,
     * nesse caso ele só faz esperar 2 segundos
     */
    do{
    //sleep(7.5);
    printf("Testando \n");
    /* Coleta a data de fim do processo*/
    gettimeofday(&Fim, NULL);

    /* Calcula o espaço de tempo */
    Segundos = Fim.tv_sec - Inicio.tv_sec;
    Microssegundos = Fim.tv_usec - Inicio.tv_usec;
   }while(Segundos!=10);
    /* Transforma segundos e microssegundos em milissegundos */
    //Milissegundos = Segundos * 1000 + Microssegundos / 1000;

    printf("O programa demourou: %.0lf:%.2lf\n",Segundos,Microssegundos );

return 0;
}