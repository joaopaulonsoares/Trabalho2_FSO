#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <ctype.h>



int main()
{
    
    pid_t child_a, child_b;
	struct timeval Inicio, Fim;
    double Segundos, Milissegundos;
    char entrada[100];
    char mensagemDorminhoco[52]="",mensagemAtivo[100],mensagemRecebidaDorminhoco[100]="",mensagemRecebidaAtivo[256];
    int contadorMensagemDor=1,contadorMensagemAtiv=1;
    int filhoDor[2],filhoAti[2];
    fd_set escritaDorminhoco,escritaAtivo,leitura;
    FILE  *pFile;
    struct timeval t;
    struct timeval tempoFinal;

    remove("output.txt");

    t.tv_sec = 0;
    t.tv_usec = 50000;

    gettimeofday(&Inicio, NULL);
    double tempoInicialSeg=Inicio.tv_sec * 1.0 +Inicio.tv_usec/1000000.00;

    if (pipe(filhoDor) == -1){
        exit(1);
    }
    if(pipe(filhoAti) == -1){
        exit(1);
    }

    FD_ZERO(&escritaDorminhoco);
    FD_ZERO(&escritaAtivo);
	FD_ZERO(&leitura);

    if (child_a = fork()) {
        if (child_b = fork()) {
        
        } 
    } 

    do{

        FD_SET(filhoDor[1], &escritaDorminhoco);
        FD_SET(filhoAti[1], &escritaAtivo);

	    if (child_a == 0) {
	        //============================== PROCESSO FILHO DORMINHOCO ============================================
	     	    int ale = rand() % 3;
	     	    //printf("********Filho Preguicoso %d Rand: %d   Seg: %lf\n",contadorMensagemDor,ale,Segundos);

		     	int retorno = select(filhoDor[1] + 1,NULL,&escritaDorminhoco, NULL,NULL);
		     	if(retorno>0){

                    gettimeofday(&tempoFinal,NULL);
                    double tempoFinalSeg=tempoFinal.tv_sec * 1.0 +tempoFinal.tv_usec/1000000.00;
                    double tempo= tempoFinalSeg - tempoInicialSeg;

		     		snprintf (mensagemDorminhoco,70, "00:%.3f : %s %d %s",tempo,"Mensagem" ,contadorMensagemDor ,"do filho dorminhoco.");
		     		contadorMensagemDor++;

		     		close(filhoDor[0]);
		     		write(filhoDor[1],mensagemDorminhoco, sizeof(mensagemDorminhoco) + 1);
		     	}

	     	    sleep(ale);
            //=======================================================================================================
	    }else {
	        if (child_b == 0) {
                //=============================== PROCESSO FILHO ATIVO ==============================================

                //printf(" ENTROU ATIVO\n");
	        	int retornoAti = select(filhoAti[1] + 1,NULL,&escritaAtivo, NULL,&t);
             	if(retornoAti>0){

             		fflush(stdin);
                    scanf("%[^\n]s",entrada);
                    setbuf(stdin, NULL);

                    gettimeofday(&tempoFinal,NULL);
                    double tempoFinalSeg=tempoFinal.tv_sec * 1.0 +tempoFinal.tv_usec/1000000.00;
                    double tempo= tempoFinalSeg - tempoInicialSeg;

 					snprintf (mensagemAtivo,70, "00:%.3lf : %s %d %s %s %s",tempo,"Mensagem" ,contadorMensagemAtiv ,"do filho Ativo: <",entrada,">");
		     		contadorMensagemAtiv++;

		     		close(filhoAti[0]);
		     		write(filhoAti[1],mensagemAtivo, sizeof(mensagemAtivo) + 1);
				}
                //=====================================================================================================
	        }else{
	             // ============================== PROCESSO PAI ======================================================== 
                FD_SET(filhoDor[0], &leitura);
                FD_SET(filhoAti[0], &leitura);

                int retornoAtivo = select(filhoAti[0]+1, &leitura, NULL, NULL,&t);
                if(retornoAtivo>0){
                    if((FD_ISSET(filhoAti[0],&leitura))>0){
                        int retAti = read(filhoAti[0],mensagemRecebidaAtivo, sizeof(mensagemRecebidaAtivo));
                        if(retAti>0){
                            if(strlen(mensagemRecebidaAtivo)>2){
                                pFile = fopen("output.txt","a+");

                                if (pFile == NULL) {
                                   printf ("Houve um erro ao abrir o arquivo.\n");
                                   return 1;
                                }

                                gettimeofday(&tempoFinal,NULL);
                                double tempoFinalSeg=tempoFinal.tv_sec * 1.0 +tempoFinal.tv_usec/1000000.00;
                                double tempo= tempoFinalSeg - tempoInicialSeg;

                                fprintf (pFile,"00:%.3f  : %s\n",tempo,mensagemRecebidaAtivo);
                                fclose(pFile);
                                FD_CLR (filhoAti[0],&leitura);
                            }
                        }
                    }
                }

     		    int retornoDorminhoco = select(filhoDor[0] + 1, &leitura, NULL, NULL,&t);
         		if(retornoDorminhoco > 0){

                    if((FD_ISSET(filhoDor[0],&leitura))>0){
             			int retDor = read(filhoDor[0],mensagemRecebidaDorminhoco, sizeof(mensagemRecebidaDorminhoco));
             			if(retDor >0){
             				if(strlen(mensagemRecebidaDorminhoco)>2){
             					pFile = fopen("output.txt","a+");
    							   	
                                if (pFile == NULL) {
    							       printf ("Houve um erro ao abrir o arquivo.\n");
    							       return 1;
    							}

                                gettimeofday(&tempoFinal,NULL);
                                double tempoFinalSeg=tempoFinal.tv_sec * 1.0 +tempoFinal.tv_usec/1000000.00;
                                double tempo= tempoFinalSeg - tempoInicialSeg;

             				  	fprintf (pFile,"00:%.3f  : %s\n",tempo,mensagemRecebidaDorminhoco);
             				  	fclose(pFile);   
                                FD_CLR (filhoDor[0],&leitura);
             				}
             			}
                    }
         		}
                // =====================================================================================================
	        }
	    }

	    gettimeofday(&Fim, NULL);   
        Segundos = Fim.tv_sec - Inicio.tv_sec;
        Milissegundos =  (Fim.tv_usec - Inicio.tv_usec);

        FD_ZERO(&escritaDorminhoco);
        FD_ZERO(&escritaAtivo);
		FD_ZERO(&leitura);

	}while(Segundos!=15);
	     
   	printf("===>Segundos = %lf\n",Segundos);

   	kill(child_a,SIGKILL);
    kill(child_b,SIGKILL);


	return 0;
}