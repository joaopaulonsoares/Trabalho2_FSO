#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <ctype.h>



int main()
{

	struct timeval Inicio, Fim;
    double Segundos, Milissegundos, Microssegundos;
    int status;
    char entrada[100];
    char mensagemDorminhoco[256]="";
    char mensagemAtivo[256];
    char mensagemRecebidaDorminhoco[256]="";
    char mensagemRecebidaAtivo[256];
    int contadorMensagemDor=1;
    int contadorMensagemAtiv=1;
    pid_t filhoDorminhoco, filhoAtivo;
    int filhoDor[2];
    int filhoAti[2];
    fd_set escrita,leitura;
    FILE  *pFile;
    struct timeval t;

    t.tv_sec = 1;
    t.tv_usec = 0;

    gettimeofday(&Inicio, NULL);

    
    if (pipe(filhoDor) == -1) {
        exit(1);
    }
    if (pipe(filhoAti) == -1) {
        exit(1);
    }

if (filhoDorminhoco = fork()) {
    if (filhoAtivo = fork()) {
        
    } 
} 

//    filhoAtivo = fork();      
  //  filhoDorminhoco = fork();

	FD_ZERO(&escrita);
	FD_ZERO(&leitura);

    do{


   		FD_SET(filhoDor[0], &leitura);
   		FD_SET(filhoDor[1], &escrita);
   		FD_SET(filhoAti[0], &leitura);
   		FD_SET(filhoAti[1], &escrita);

	    if (filhoDorminhoco == 0) {
	          //PROCESSO DORMINHOCO
	     	    int ale = rand() % 3;
	     	    //printf("********Filho Preguicoso %d Rand: %d   Seg: %lf\n",contadorMensagemDor,ale,Segundos);

		     	int retorno = select(filhoDor[1] + 1,NULL,&escrita, NULL,NULL);
		     	if(retorno>0){
        			
		     		snprintf (mensagemDorminhoco, 300, "%s%.0f%s%s%.3f%s %s %d %s","0",Segundos,":","0",Milissegundos,":","Mensagem" ,contadorMensagemDor ,"do filho dorminhoco.");
		     		contadorMensagemDor++;
		     		//close(filhoDor[0]);
		     		write(filhoDor[1],mensagemDorminhoco, sizeof(mensagemDorminhoco) + 1);
	     	    //printf(" %s\n",mensagemDorminhoco);
		     	}

	     	    sleep(ale);
	    } else {
	  
	        if (filhoAtivo == 0) {
	           
	             // PROCESSO ATIVO
	             //printf("+++++++++Filho Ativo\n");
             	int retornoAti = select(filhoAti[1] + 1,NULL,&escrita, NULL,&t);
             	if(retornoAti>0){
             		fflush(stdin);
	         		scanf("%s",entrada);
 					//snprintf (mensagemAtivo, 256, "%s %d %s %s","Mensagem",contadorMensagemAtiv ,"do usuario: ",a);
 					snprintf (mensagemAtivo, 300, "%.0f%s%.3f%s %s %d %s %s %s",Segundos,":",Milissegundos,":","Mensagem" ,contadorMensagemAtiv ,"do filho Ativo: <",entrada,">");
		     		contadorMensagemAtiv++;
		     		close(filhoAti[0]);
		     		write(filhoAti[1],mensagemAtivo, sizeof(mensagemAtivo) + 1);
	                // printf("O programa demorou %.3lf segundos para terminar\n",Segundos);
				}

	        } else {
	             	//PROCESSO PAI
	         	 	//printf("=============Pai\n");
	         	 	//sleep(1);

     		    int retornoDorminhoco = select(filhoDor[0] + 1, &leitura, NULL, NULL,&t);
     		    
         		if(retornoDorminhoco > 0){
         			//close(filhoDor[1]);
         			int retDor = read(filhoDor[0],mensagemRecebidaDorminhoco, sizeof(mensagemRecebidaDorminhoco));
         			if(retDor >0){
         				if(strlen(mensagemRecebidaDorminhoco)>2){
         					//printf("%s\n",mensagemRecebidaDorminhoco);
         					pFile = fopen("myfile2.txt","a+");
							   	 if (pFile == NULL) {
							       printf ("Houve um erro ao abrir o arquivo.\n");
							       return 1;
							    }
         				  	fprintf (pFile,"%.0f%s%.3f%s %s\n",Segundos,":",Milissegundos,":",mensagemRecebidaDorminhoco);
         				  	fclose(pFile);   
         				}
         			}
         		}else{

		        	int retornoAtivo = select(filhoAti[0] + 1, &leitura, NULL, NULL,&t);
	         		if(retornoAtivo>0){
	         			int retornoAti = read(filhoAti[0],mensagemRecebidaAtivo, sizeof(mensagemRecebidaAtivo));
	         			if(retornoAti>0){
	         				if(strlen(mensagemRecebidaAtivo)>2){
	         					//printf("%s\n",mensagemRecebidaAtivo);
	         					 pFile = fopen("myfile2.txt","a+");
							   	 if (pFile == NULL) {
							       printf ("Houve um erro ao abrir o arquivo.\n");
							       return 1;
							    }
							    fprintf (pFile,"%s\n",mensagemRecebidaAtivo);
							    fclose(pFile);
	         				}
	         			}
	         		}

         		}
	        }
	    }
	

	    gettimeofday(&Fim, NULL);   
        Segundos = Fim.tv_sec - Inicio.tv_sec;
        Milissegundos =  (Fim.tv_usec - Inicio.tv_usec)/1000.0;
   	}while(Segundos!=10);


   	printf("===>Segundos = %lf\n",Segundos);

   	kill(filhoDorminhoco,SIGKILL);
   	kill(filhoAtivo,SIGKILL);


return 0;

}